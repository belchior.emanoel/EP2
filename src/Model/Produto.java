package Model;

public class Produto {
        private String nome;
        private Float valor;
        private Integer estoque;
        private Integer minimoEstoque=5;

    public Produto() {
    }

    public Produto(String nome, Float valor, Integer estoque) {
        this.nome = nome;
        this.valor = valor;
        this.estoque = estoque;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Float getValor() {
        return valor;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }

    public Integer getEstoque() {
        return estoque;
    }

    public void setQuantidade(Integer estoque) {
        this.estoque = estoque;
    }               

    public Integer getMinimoEstoque() {
        return minimoEstoque;
    }
}
